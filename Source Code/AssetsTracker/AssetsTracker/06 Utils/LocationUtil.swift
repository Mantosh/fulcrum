//
//  LocationUtil.swift
//  Salesgenie Phase 1
//
//  Created by Kumar C on 5/31/16.
//  Copyright © 2016 Kumar C. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit



class LocationUtil : NSObject {
    
    static let Global = LocationUtil ();
    
    var handler : Completion.Location?
    
    var userLocation : CLLocationCoordinate2D
    
    let noLocation = CLLocationCoordinate2D (latitude: 0, longitude: 0)

    var locationManager : CLLocationManager;
    
    var hasSentLocationUpdate = false;
    
    var hasShownAlert = false

    var waitingForPermission = false
    
    var needUserAction = false
    
    private (set) var userActionMessage = ""
    
    weak var alertViewController : UIViewController?
    
    override init ()
    {
        userLocation = CLLocationCoordinate2D (latitude: 0, longitude: 0);

        locationManager = CLLocationManager ();

        super.init()
        
        locationManager.delegate = self;
        
        sendAuthErrorIfAny(CLLocationManager.authorizationStatus())

        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    func applicationDidBecomeActive(_ notification : Notification?) -> Void {
        if waitingForPermission {
            waitingForPermission = false
            sendAuthErrorIfAny(CLLocationManager.authorizationStatus())
        }
    }
    
    func reverseGeocode(_ location2D : CLLocationCoordinate2D, completion : @escaping Completion.Geocode) -> Void {
        
        let location = CLLocation (latitude: location2D.latitude, longitude: location2D.longitude)
        
        let geoCoder = CLGeocoder ();
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            
            if error == nil && placemarks != nil && placemarks!.count > 0
            {
                completion (true, placemarks?.last, nil);
            }
            else
            {
                completion (false, nil, MKError (_nsError: error! as NSError).code.message());
            }
        })
    }
    
    func startNavigation (for location2D : CLLocationCoordinate2D, completion : @escaping Completion.Default) -> Void {
        
        // http://maps.apple.com/maps?saddr=Current%20Location&daddr=<Your Location>
        
        reverseGeocode(location2D) { (success, placemark, responseMessage) in
            completion (success, responseMessage)
            guard placemark != nil && success else
            {
                return
            }
            
            let mapRegion = MKCoordinateRegion (center: location2D, span: MKCoordinateSpan (latitudeDelta: 0.5, longitudeDelta: 0.5))
            
            let mapItem = MKMapItem (placemark: MKPlacemark.init(placemark: placemark!))
            
            var launchOptions : [String : Any]
            
            if #available(iOS 10.0, *) {
                launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDefault, MKLaunchOptionsMapSpanKey : mapRegion.span, MKLaunchOptionsShowsTrafficKey : true]
            } else {
                // Fallback on earlier versions
                launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapSpanKey : mapRegion.span, MKLaunchOptionsShowsTrafficKey : true]
            }
            mapItem.openInMaps(launchOptions: launchOptions)
        }
    }
    
    func reverseGeocodeUserLocation(withCompletion completion : @escaping Completion.Geocode) -> Void {
        reverseGeocode(userLocation, completion: completion);
    }
    
    func getCurrentLocation(showAlert : Bool = true, withCompletion completion : @escaping Completion.Location) -> Void {
        
        let shouldUseSimulatedLocation : Bool = Bundle.main.object(forInfoDictionaryKey: "ShouldUseSimulatedLocation") as! Bool
        
        let isFakeLocationEnabled = UserDefaults.standard.bool(forKey: "IsFakeLocationEnabled")
        
        if shouldUseSimulatedLocation {
            
            let from = CLLocationCoordinate2D (latitude: 41.1544, longitude: -95.9146);
            let to = CLLocationCoordinate2D (latitude: 41.1544, longitude: -95.9146);
            userLocation = to;
            
            completion (true, from, to, nil);
            
            return;
        }
        else if isFakeLocationEnabled
        {
            let latString = UserDefaults.standard.string(forKey: "FakeLatitude")
            let lngString = UserDefaults.standard.string(forKey: "FakeLongitude")
            
            if let latString = latString, let lngString = lngString {
                let longitude = Double (lngString)
                let latitude = Double (latString)
                
                let location = CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude: longitude ?? 0.0)
                
                if CLLocationCoordinate2DIsValid(location) {
                    userLocation = location
                    completion (true, location, location, nil);
                    return;
                }
            }
        }
        
        handler = completion;

        hasShownAlert = !showAlert
        
        hasSentLocationUpdate = false;
        
        sendAuthErrorIfAny(CLLocationManager.authorizationStatus());
        
    }
    
    func sendAuthErrorIfAny(_ authStatus : CLAuthorizationStatus) -> Void {
        
        guard CLLocationManager.locationServicesEnabled() else
        {
            needUserAction = true
            userActionMessage = "Location service is disabled. Please go to Settings-> Privacy-> Location"
            sendLocation(success: false, fromLocation: noLocation, toLocation: noLocation, error: userActionMessage);
            return;
        }
        
        needUserAction = false
        switch authStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation();
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization();
        case .denied:
            needUserAction = true
            userActionMessage = "Location service is disabled for Salesgenie. Please go to Settings-> Salesgenie-> Location Services and set it to When In Use"
            sendLocation(success: false, fromLocation: noLocation, toLocation: noLocation, error: userActionMessage);
        case .restricted:
            needUserAction = true
            userActionMessage = "Location service is restricted. Please go to Settings-> General-> Restrictions-> Location Services to disable restrictions"
            sendLocation(success: false, fromLocation: noLocation, toLocation: noLocation, error: userActionMessage);
        }
    }
    
    func sendLocation(success : Bool, fromLocation : CLLocationCoordinate2D, toLocation : CLLocationCoordinate2D, error : String?) -> Void {
        
        if waitingForPermission {
            return
        }
        
        if !hasSentLocationUpdate && handler != nil {
            
            if needUserAction && !hasShownAlert {
                hasShownAlert = true
                waitingForPermission = true
                showLocationAlert(withMessage: error)
                return
            }
            
            userLocation = toLocation;
            hasSentLocationUpdate = true;
            
            locationManager.stopUpdatingLocation()
            
            let handler2 = handler
            handler = nil
            
            hasShownAlert = false
            
            alertViewController = nil
            
            handler2?(success, fromLocation, toLocation, error);
        }
    }
    
    func showLocationAlert(withMessage message : String?) -> Void {
        
        if let message = message {
            let alertController = UIAlertController (title: "Message", message: message, preferredStyle: .alert)
            
            let settingsAction = UIAlertAction (title: "Settings", style: .default) { [weak self] (action) in
                
                self?.waitingForPermission = true
                
                if CLLocationManager.locationServicesEnabled() == false
                {
                    UIApplication.shared.openURL(URL (string: "App-Prefs:root=Privacy&path=LOCATION")!)
                }
                else
                {
                    UIApplication.shared.openURL(URL (string: UIApplicationOpenSettingsURLString)!)
                }
            }
            
            let cancelAction = UIAlertAction (title: "Cancel", style: .default, handler: { [weak self] (action) in
                self?.waitingForPermission = false
                self?.sendLocation(success: false, fromLocation: self!.noLocation, toLocation: self!.noLocation, error: message)
            })
            
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)
            
            if let alertViewController = alertViewController {
                alertViewController.present(alertController, animated: true, completion: nil)
            }
            else
            {
                //Salesgenie.applicationDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
        else
        {
            waitingForPermission = false
            hasShownAlert = false
        }
    }
}

extension LocationUtil : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        sendAuthErrorIfAny(CLLocationManager.authorizationStatus());
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if !hasSentLocationUpdate && handler != nil {
            
            let currentError = CLError (_nsError: error as NSError)
            
            sendAuthErrorIfAny(CLLocationManager.authorizationStatus());
            
            if !needUserAction {
                waitingForPermission = false
                sendLocation(success: false, fromLocation: noLocation, toLocation: noLocation, error: currentError.code.message())
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        waitingForPermission = false
        
        sendLocation(success: true, fromLocation: locations [0].coordinate, toLocation: locations.last.coordinate, error: nil);
    }
}

func * (left: String, right: Int) -> String {
    if right <= 0 {
        return ""
    }
    
    var result = left
    for _ in 1..<right {
        result += left
    }
    
    return result
}

precedencegroup Additive {
    associativity: left
    higherThan: AdditionPrecedence
}

infix operator --> : Additive
extension CLLocationCoordinate2D
{
    static func --> (from2D : CLLocationCoordinate2D, to2D : CLLocationCoordinate2D) -> CLLocationDistance {
        
        let fromLocation = CLLocation (latitude: from2D.latitude, longitude: from2D.longitude);
        let toLocation = CLLocation (latitude: to2D.latitude, longitude: to2D.longitude);
        
        return fromLocation.distance(from: toLocation);
    }
}

extension CLError.Code
{
    func message() -> String {
        
        var errorMessage : String = "Unable to obtain location services at this time. Please try again"
        
        switch (self) {
        case .deferredAccuracyTooLow:
            errorMessage = "Deferred mode is not supported for the requested accuracy";
        case .deferredCanceled:
            errorMessage = "The request for deferred updates was canceled by the app or by the location manager";
        case .deferredDistanceFiltered:
            errorMessage = "Deferred mode does not support distance filters";
        case .deferredFailed:
            errorMessage = "GPS is unavailable, not active, or is temporarily interrupted";
        case .deferredNotUpdatingLocation:
            errorMessage = "Location updates were disabled or paused. Plese try again";
        case .denied:
            errorMessage = "Location service is disabled for this app. Please go to app settings and enable location service";
        case .geocodeCanceled:
            errorMessage = "The geocode request was canceled";
        case .geocodeFoundNoResult:
            errorMessage = "The geocode request yielded no result. Please try with a different keyword";
        case .geocodeFoundPartialResult:
            errorMessage = "The geocode request yielded a partial result. Please try with a different keyword";
        case .headingFailure:
            errorMessage = "The heading could not be determined. Please try again";
        case .locationUnknown:
            errorMessage = "Unable to obtain a location value right now. Please try again";
        case .network:
            errorMessage = "The network was unavailable or a network error occurred. Please try again";
        case .rangingFailure:
            errorMessage = "A general ranging error occurred. Please try again";
        case .rangingUnavailable:
            errorMessage = "Ranging is disabled. This might happen if your device is in Airplane mode or if Bluetooth or location services are disabled";
        case .regionMonitoringDenied:
            errorMessage = "Access to the region monitoring service was denied by the user";
        case .regionMonitoringFailure:
            errorMessage = "A registered region cannot be monitored or the region’s radius distance is too large";
        case .regionMonitoringResponseDelayed:
            errorMessage = "Unable to obtain a location value right now. Please try again";
        case .regionMonitoringSetupDelayed:
            errorMessage = "Could not initialize the region monitoring feature immediately. Please try again";
        }
        return errorMessage;
    }
}

extension Array {
    var last: Element {
        return self[self.endIndex - 1]
    }
}

extension MKError.Code
{
    func message() -> String {
        
        var errorMessage : String = "Unable to obtain map services at this time. Please try again"
        
        switch self {
        case .unknown:
            errorMessage = "An unknown error occurred. Please try again"
        case .serverFailure:
            errorMessage = "The map server was unable to return the desired information. Please try again"
        case .loadingThrottled:
            errorMessage = "The data was not loaded because data throttling is in effect. Please try again"
        case .directionsNotFound:
            errorMessage = "The specified directions could not be found"
        case .placemarkNotFound:
            errorMessage = "The search did not yield any results. Please make sure the search info is correct, or try with a different search info"
        }
        return errorMessage;

    }
}
