//
//  RequestGateway.swift
//  Salesgenie Phase 1
//
//  Created by Kumar C on 6/16/16.
//  Copyright © 2016 Kumar C. All rights reserved.
//

import Foundation
import Alamofire

final class RequestGateway: RequestManager {
    
    override func jsonRequest (withUrl url : URL, method : HTTPMethod, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, postData : [String : Any]? = nil, completion : @escaping RequestHandler) -> Request? {
        let serviceRequest = super.jsonRequest(withUrl: url as URL, method: method, urlParameters: params, headers: headers, postData: postData, completion: { (jsonData, error, statusCode) in
            
            if (statusCode == Constants.InvalidUserCode )
            {
                
            }
        });
        
        return serviceRequest;
    }
}
