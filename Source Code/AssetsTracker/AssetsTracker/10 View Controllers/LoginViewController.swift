//
//  LoginViewController.swift
//  AssetsTracker
//
//  Created by Mantosh Kumar on 9/15/17.
//  Copyright © 2017 ThirdEye. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import NVActivityIndicatorView


class LoginViewController: UIViewController, NVActivityIndicatorViewable {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getRegster()
    }

    func getRegster() -> Void {
        
        startAnimating(CGSize(width: 50, height:50), message: "", type: NVActivityIndicatorType.lineScalePulseOut)
        
        AccountManager.Global.getSecureToken { [weak self] (success, jsonData, responseMessage) in
            
            self?.stopAnimating()
            
            print("responce data :- \(String(describing: jsonData))")
            
            if let json = jsonData
            {
                let newSecureToken = json ["Response"]["responseDetails"]["Secure Token"].stringValue
                
                print(newSecureToken)
                
                //self?.getAuthorised(newSecureToken: newSecureToken)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
