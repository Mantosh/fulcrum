//
//  AppDelegate.swift
//  AssetsTracker
//
//  Created by Mantosh Kumar on 9/15/17.
//  Copyright © 2017 ThirdEye. All rights reserved.
//

import UIKit
import XCGLogger
import SDCAlertView
import Crashlytics
import OneSignal
import Fabric

let logger = XCGLogger.default

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let redViewController = AssetsListViewController()
    let greenViewController = AssetsListViewController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // For debugging
        //OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            print("Received Notification: \(notification!.payload.notificationID)")
            print("launchURL = \(String(describing: notification?.payload.launchURL))")
            print("content_available = \(String(describing: notification?.payload.contentAvailable))")
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload? = result?.notification.payload
            
            print("Message = \(payload!.body)")
            print("badge number = \(String(describing: payload?.badge))")
            print("notification sound = \(String(describing: payload?.sound))")
            
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "AssetsListViewController") as! AssetsListViewController
            let navigationController = UINavigationController.init(rootViewController: viewController)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()

            
            if let additionalData = result!.notification.payload!.additionalData {
                
                print("additionalData = \(additionalData)")
                
                // DEEP LINK and open url in RedViewController
                // Send notification with Additional Data > example key: "OpenURL" example value: "https://google.com"
                
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "AssetsListViewController") as! AssetsListViewController
                let navigationController = UINavigationController.init(rootViewController: viewController)
                self.window?.rootViewController = navigationController
                self.window?.makeKeyAndVisible()

                
                if let actionSelected = payload?.actionButtons {
                    print("actionSelected = \(actionSelected)")
                }
                
                // DEEP LINK from action buttons
                if let actionID = result?.action.actionID {
                    
                    // For presenting a ViewController from push notification action button
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let instantiateRedViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "AssetsListViewController") as UIViewController
                    let instantiatedGreenViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "AssetsListViewController") as UIViewController
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    
                    print("actionID = \(actionID)")
                    
                    if actionID == "id2" {
                        print("do something when button 2 is pressed")
                        self.window?.rootViewController = instantiateRedViewController
                        self.window?.makeKeyAndVisible()
                        
                        
                    } else if actionID == "id1" {
                        print("do something when button 1 is pressed")
                        self.window?.rootViewController = instantiatedGreenViewController
                        self.window?.makeKeyAndVisible()
                        
                    }
                }
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: true ]
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: "036b8707-5c34-47f1-bd91-cf1d3870a670", handleNotificationReceived: notificationReceivedBlock, handleNotificationAction: notificationOpenedBlock, settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
                
        registerOneSingle()
        
        return true
    }
    
    
    //=================
    
    func displaySettingsNotification() {
        let message = NSLocalizedString("Please turn on notifications by going to Settings > Notifications > Allow Notifications", comment: "Alert message when the user has denied access to the notifications")
        let alertController = UIAlertController(title: "OneSignal Example", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        //self.present(alertController, animated: true, completion: nil)
    }

    
    func registerOneSingle() -> Void {
        
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let hasPrompted = status.permissionStatus.hasPrompted
        if hasPrompted == false {
            // Call when you want to prompt the user to accept push notifications.
            // Only call once and only if you set kOSSettingsKeyAutoPrompt in AppDelegate to false.
            OneSignal.promptForPushNotifications(userResponse: { accepted in
                if accepted == true {
                    print("User accepted notifications: \(accepted)")
                } else {
                    print("User accepted notifications: \(accepted)")
                }
            })
        } else {
            displaySettingsNotification()
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    /*
    /// Initializes OneSIgnal with App Id and App Secret
    ///
    /// - Parameters:
    ///   - application: Application
    ///   - launchOptions: Launch options while launching the app
    private func startOneSignalFor (application : UIApplication, launchOptions: [AnyHashable: Any]?) -> Void
    {
        if UIDevice.isSimulator {
            return
        }
        let oneSignalAppId = Bundle.main.object(forInfoDictionaryKey: Constants.OneSignalKey) as? String
        
        if let oneSignalAppId = oneSignalAppId {
            
            let settings = [kOSSettingsKeyAutoPrompt            : true,
                            kOSSettingsKeyInAppAlerts           : false]
            
            OneSignal.initWithLaunchOptions(launchOptions, appId: oneSignalAppId, handleNotificationReceived: { [weak self] (notification) in
                
                let payload = notification?.payload
                
                if let dispatchInfo = payload?.additionalData
                {
                    BMSPushManager.Global.parsePayLoad(from: dispatchInfo)
                }
                
                UIApplication.shared.applicationIconBadgeNumber = Int (payload?.badge ?? 0)
                
                //                let isNotOpen = application.applicationState == .inactive || application.applicationState == .background
                
                }, handleNotificationAction: { [weak self] (result) in
                    
                    // This block gets called when the user reacts to a notification received
                    let payload = result?.notification.payload
                    //                    var fullMessage = payload?.title
                    
                    if let dispatchInfo = payload?.additionalData
                    {
                        BMSPushManager.Global.parsePayLoad(from: dispatchInfo)
                    }
                    
                    //                    //Try to fetch the action selected
                    //                    if let additionalData = payload?.additionalData, let actionSelected = additionalData["actionSelected"] as? String {
                    //                        fullMessage =  fullMessage! + "\nPressed ButtonId:\(actionSelected)"
                    //                    }
                    
                }, settings: settings)
        }
        else
        {
            fatalError("\n\nOne signal app id with key \"OneSignalAppID\" not found in 'Info.plist' file\n\n")
        }
    }
    
    */


}

