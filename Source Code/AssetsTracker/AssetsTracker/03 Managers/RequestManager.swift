//
//  RequestManager.swift
//  AtoZdatabase
//
//  Created by Mantosh Kumar on 8/23/17.
//  Copyright © 2017 ThirdEye. All rights reserved.
//

import UIKit
import Alamofire
import JASON
import ReachabilitySwift

class RequestManager: NSObject {
    
    static let Global = RequestManager()
    
    let reachability = Reachability()!
    
    private (set) var isConnectedToInternet = true
    
    typealias RequestHandler = (_ jsonData : JSON? , _ error : String?, _ statusCode : Int) -> Void

    struct RestURLPath {
        
        init()
        {
            
        }
        private(set) var host : String = ""
        
        init (fromPropertyFile propertyFileName : String)
        {
            let path : String! = Bundle.main.path(forResource: propertyFileName, ofType: "plist")
            
            let preferences : NSDictionary! = NSDictionary(contentsOfFile: path)
            
            host = preferences.value(forKey: "Host") as! String
        }
    }
    
    private (set) var baseUrlPath : RestURLPath!
    private (set) var environment : String
    private var alamoFireManager : Alamofire.SessionManager = Alamofire.SessionManager.default
    
    override init ()
    {
        //FIXME: Change to proper environment while deploying.
        
        switch (BuildEnvironment.current)
        {
        case .adhoc, .development:
            baseUrlPath = RestURLPath (fromPropertyFile: "ATServices_Prod")
        case .production:
            baseUrlPath = RestURLPath (fromPropertyFile: "ATServices_Prod")
        }
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.RequestTimeOut // seconds
        configuration.timeoutIntervalForResource = Constants.RequestTimeOut
        
        environment = baseUrlPath.host.contains("test") ? "TEST" : "PROD"
        
        super.init()
        
        reachability.whenReachable = { reachability in
            DispatchQueue.global(qos: .background).async { [weak self] in
                if reachability.isReachableViaWiFi {
                    logger.debug("Reachable via WiFi")
                } else {
                    logger.debug("Reachable via Cellular")
                }
                self?.isConnectedToInternet = true
            }
        }
        reachability.whenUnreachable = { reachability in
            
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.isConnectedToInternet = false
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            logger.debug("Unable to start notifier")
        }
        
        UserDefaults.standard.addObserver(self, forKeyPath: "IsProductionEnvironment", options: .new, context: nil)
    }
    
    /// Performs REST API call where the response is expected to be in JSON
    ///
    /// - Parameters:
    ///   - url: APi endpoint
    ///   - method: Request method
    ///   - params: Url parameters
    ///   - headers: Request headers
    ///   - postData: Post data
    ///   - completion: Callback
    /// - Returns: Alamofire request
    func jsonRequest (withUrl url : URL, method : HTTPMethod, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, postData : [String : Any]? = nil, completion : @escaping RequestHandler) -> Request? {
        
        if !isConnectedToInternet {
            completion (nil, "Internet not connected", 0)
            return nil
        }
        
        var urlString = url.absoluteString
        
        if params != nil && postData != nil {
            
            let encoding : ParameterEncoding = URLEncoding.queryString
            
            let mutableURLRequest = try! URLRequest (url: url.absoluteString, method: .get, headers: headers)
            let encodedURLRequest = try! encoding.encode(mutableURLRequest, with: params)
            
            urlString = encodedURLRequest.url!.absoluteString
        }
        
        var encoding : ParameterEncoding = URLEncoding.httpBody
        
        var requestParams : [String : Any]?
        
        switch method {
        case .get, .delete:
            encoding = Alamofire.URLEncoding.queryString
            requestParams = params
        case .post, .put:
            encoding = JSONEncoding.init(options: .init(rawValue: 0))
            requestParams = postData
        default:
            encoding = URLEncoding.methodDependent
            requestParams = postData
        }
        
        let reqTime = Date()
        let request = alamoFireManager.request(urlString, method: method, parameters: requestParams, encoding: encoding, headers: headers)
        
        request.responseJSON { [weak self] (responseObject) in
            
            let result = responseObject.result
            
            let statusCode : Int = responseObject.response?.statusCode ?? 0
            
            logger.debug("Status Code : \(statusCode)")
            
            if method == .post
            {
                logger.debug(String.init(data: request.request!.httpBody!, encoding: .utf8)?.removingPercentEncoding)
            }
            
            switch(result) {
            case .success(let value):
                guard let jsonData : JSON = JSON (value) else
                {
                    // Service Errors
                    let errorMessage = self?.getMessage(forHTTPStatusCode: statusCode)
                    completion (nil, errorMessage, statusCode)
                    return
                }
                
                completion (jsonData, nil, statusCode)
                
            case .failure(let error):
                // Normal HTTP Error
                completion (nil, statusCode > 0 ? self?.getMessage (forHTTPStatusCode: statusCode) : error.localizedDescription, statusCode)
            }
            
            let duration = String(format: "%.5f", Date().timeIntervalSince(reqTime))
            logger.debug ("\n\n API Execution Time : \(duration)")
        }
        
        if let innerRequest = request.request, let requestUrl = innerRequest.url {
            
            var requestLog = "\n\n API Hit:  \n\n \(requestUrl) \n\n Method: \(method.rawValue)\n\n"
            
            if let headers = headers, headers.count > 0 {
                requestLog.append(" Headers : \n\n \(headers)\n\n")
            }
            
            if let postData = postData {
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: postData, options: .prettyPrinted)
                    
                    if let theJSONText = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue)
                    {
                        requestLog.append(" Post Data : \n\n \(theJSONText)\n\n")
                    }
                    
                } catch {
                    print(error.localizedDescription)
                }
            }
            
            logger.debug(requestLog)
        }
        
        if request.request != nil {
        }
        
        return request
        
    }
    
    func JsonGetRequest(withURL url : URL, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, completion : @escaping RequestHandler) -> Request? {
        
        return jsonRequest(withUrl: url, method: .get, urlParameters: params, headers: headers, postData: nil, completion: completion)
    }
    
    func JsonDeleteRequest(withURL url : URL, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, completion : @escaping RequestHandler) -> Request? {
        return jsonRequest(withUrl: url, method: .delete, urlParameters: params, headers: headers, postData: nil, completion: completion)
    }
    
    func JsonPostRequest(withUrl url : URL, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, postData : [String : Any]?, completion : @escaping RequestHandler) -> Request?
    {
        return jsonRequest(withUrl: url, method: .post, urlParameters: params, headers: headers, postData: postData, completion: completion)
    }
    
    func JsonPutRequest(withUrl url : URL, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, postData : [String : Any]?, completion : @escaping RequestHandler) -> Request?
    {
        return jsonRequest(withUrl: url, method: .put, urlParameters: params, headers: headers, postData: postData, completion: completion)
    }
    
    func JsonDeleteRequest(withUrl url : URL, urlParameters params : [String : Any]? = nil, headers : [String : String]? = nil, completion : @escaping RequestHandler) -> Request?
    {
        return jsonRequest(withUrl: url, method: .delete, urlParameters: params, headers: headers, postData: nil, completion: completion)
    }
    
    
    /// Utility method to return formatted response message based on HTTP status code
    ///
    /// - Parameter statusCode: HTTP status code
    /// - Returns: Response message
    func getMessage(forHTTPStatusCode statusCode : Int) -> String {
        
        var errorMessage : String
        
        switch (statusCode)
        {
        case 204:
            errorMessage = "No data recieved. Please try again"
        case 300, 301, 302, 303, 304, 305, 306, 307:
            errorMessage = "Temporary redirect error. Please try again"
        case 400: // Bad request. The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.
            errorMessage = "The request couldn't be fulfilled at this time. Please try again"
        case 401: // Unauthorized. The request requires user authentication. The response MUST include a WWW-Authenticate header field (section 14.47) containing a challenge applicable to the requested resource
            errorMessage = "Unauthorized request. Please logout and login again"
        case 403: // Forbidden - The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.
            errorMessage = "We can't proceed at this moment. Please try again"
        case 404: // Not Found - The server has not found anything matching the Request-URI. No indication is given of whether the condition is temporary or permanent.
            errorMessage = "Service not found. Please contact our support or install an update from App Store"
        case 405: // Method Not Allowed - The method specified in the Request-Line is not allowed for the resource identified by the Request-URI. The response MUST include an Allow header containing a list of valid methods for the requested resource.
            errorMessage = "Method not allowed. Please contact our support or install an update from App Store"
        case 406: // Not Acceptable - The resource identified by the request is only capable of generating response entities which have content characteristics not acceptable according to the accept headers sent in the request.
            errorMessage = "Request not accepted by server. Please contact our support or install an update from App Store"
        case 408:
            errorMessage = "Request timeout. Please try again"
        case 409:
            errorMessage = "Conflict. The request could not be completed due to a conflict with the current state of the resource. Please try again"
        case 410:
            errorMessage = "Gone. The requested resource is no longer available at the server. Please try again"
        case 411: // The server refuses to accept the request without a defined Content- Length. The client MAY repeat the request if it adds a valid Content-Length header field containing the length of the message-body in the request message.
            errorMessage = "The server refused to accept the request. . Please try again"
        case 412:
            errorMessage = "Precondition Failed. Please try again"
        case 413:
            errorMessage = "Request Entity Too Large. Please try again"
        case 415:
            errorMessage = "Unsupported Media Type. Please contact our support or install an update from App Store"
        case 416:
            errorMessage = "Requested Range Not Satisfiable. Please contact our support or install an update from App Store"
        case 417:
            errorMessage = "Expectation Failed. Please try again"
        case 500:
            errorMessage = "We're having some issues with our services. Please try again"
        case 501: // Not implemented
            errorMessage = "The request is no longer valid. Please contact our support or install an update from App Store"
        case 502:
            errorMessage = "We're having some issues with our services. Please try again"
        case 503:
            errorMessage = "We're having some issues with our services. Please try again"
        case 504:
            errorMessage = "Request timed out. Please try again"
            
        default:
            errorMessage = "Unable to process the request right now"
            break
        }
        return errorMessage
    }
}

extension Dictionary where Key: CustomDebugStringConvertible, Value:CustomDebugStringConvertible {
    
    var prettyprint : String {
        for (key,value) in self {
            print("\(key) = \(value)")
        }
        
        return self.description
    }
}

