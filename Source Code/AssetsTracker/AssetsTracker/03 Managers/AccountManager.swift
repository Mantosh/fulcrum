//
//  AccountManager.swift
//  AtoZdatabase
//
//  Created by Mantosh Kumar on 8/23/17.
//  Copyright © 2017 ThirdEye. All rights reserved.
//

import UIKit
import JASON
import CoreTelephony
import CoreLocation

class AccountManager: NSObject {
    
    static let Global = AccountManager()

    let requestManager : RequestManager
    
    
    override init() {
        
        requestManager = RequestManager.Global
        
        super.init()
        
    }
    
    
    func getSecureToken(complition : @escaping Completion.GetRegistor) -> Void {
        
        let header : [String : String] = [HTTPHeaders.ContentType : MimeType.Json]
        
        let vendorUUID = UIDevice.current.identifierForVendor?.uuidString
        let postData : [String : String] = ["IMEI" : vendorUUID!,
                                            "Subscriber" : "atoz",
                                            "SystemVersion" : UIDevice.current.systemVersion,
                                            "UDID" :  vendorUUID!,
                                            "model" : UIDevice.current.model,
                                            "LocalizedModel" : UIDevice.current.localizedModel
                                            ]
        
        var urlString = URL(string: requestManager.baseUrlPath.host)
        urlString?.appendPathComponent("mobile/register")
        
        let _ = requestManager.JsonPostRequest(withUrl: urlString!, urlParameters: nil, headers: header, postData: postData) { (jsonData, error, responceCode) in
            
            //response Data :
            
            print("JsonData :- \(String(describing: jsonData))")
            print("ResponceCode :- \(responceCode)")
            
            DispatchQueue.main.async(execute: { 
                
                if error != nil
                {
                    complition(false, nil, "error message")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    complition(true, jsonData, "")
                }
            })
        }
    }
}
