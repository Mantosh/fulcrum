//
//  DateExtension.swift
//  Salesgenie Phase 1
//
//  Created by Kumar C on 9/18/16.
//  Copyright © 2016 Kumar C. All rights reserved.
//

import Foundation

extension Date
{
    init? (fromJson string : String?)
    {
        
        if let string = string, string.characters.count > 0 {
            var subString = string [string.index(string.startIndex, offsetBy: 6)...string.index(string.endIndex, offsetBy: -3)]
            
//            // new code for handling offset
//            if (subString[subString.characters.count - 5] == "+") || (subString[subString.characters.count - 5] == "-")
//            {
//                if subString[subString.characters.count - 5 ] == "+"
//                {
//                    let dateString = subString.components (separatedBy: "+")
//                    let firstDatePart =  dateString [0]
//                    let lastDatePart =  dateString [1]
//                    
//                    let hMills = String(lastDatePart.characters.prefix(2))
//                    let mMills = String(lastDatePart.characters.suffix(2))
//                    
//                    let inthMills = Int(hMills)
//                    let intmMills = Int(mMills)
//                    let intMillsFirstDatePart = Int(firstDatePart)
//
//                    let subString1 = (intMillsFirstDatePart)! + (inthMills! * 3600000) + (intmMills! * 60000)
//                    
//                    subString = String(subString1)
//                }
//                else
//                {
//                    let dateString = subString.components (separatedBy: "-")
//                    let firstDatePart =  dateString [0]
//                    let lastDatePart =  dateString [1]
//                    
//                    let hMills = String(lastDatePart.characters.prefix(2))
//                    let mMills = String(lastDatePart.characters.suffix(2))
//                                        
//                    let hMills1 = Int(hMills)
//                    let mMills1 = Int(mMills)
//                    let MillsFirstDatePart = Int(firstDatePart)
//                    
//                    let subString1 = (MillsFirstDatePart)! - (hMills1! * 3600000) + (mMills1! * 60000)
//                    
//                    subString = String(subString1)
//                }
//            }
//            //--- 
            
            let timeStamp = Double (subString)! / 1000.0
            
            self.init (timeIntervalSince1970: timeStamp)
        }
        else
        {
            return nil
        }
    }
    
    func daySuffix() -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: self)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
}
