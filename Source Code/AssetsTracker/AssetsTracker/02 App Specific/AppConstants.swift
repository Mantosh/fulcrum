//
//  AppConstants.swift
//  AtoZdatabase
//
//  Created by Mantosh Kumar on 8/22/17.
//  Copyright © 2017 ThirdEye. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import JASON

/**
 *  Frequently used constants in AtoZdatabase
 */
struct Constants {

    static let RequestTimeOut : Double = 60.0
    
    static let SGAccountKey = "SGAccountKey2"
    
    static let PageSize = 50
    
    static let ValidResponseCode = 200
    
    static let NoResultsResponseCode = 500
    
    static let InvalidUserCode = 401
    
    static let NotFoundCode = 404
    
    static let MetersPerMile = 1609.344
    
    static let AscendingOrder = "ascending"
    
    static let DescendingOrder = "descending"
    
    static let RegForValidString = CharacterSet(charactersIn: "₹1234567890€!@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
    static let RegForValidNumber = CharacterSet(charactersIn: "₹€!@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
    
    static let TermOfUseUrl = "https://www.AtoZdatabase.com/terms-and-conditions/"
    static let PrivacyPolicyUrl = "https://www.AtoZdatabase.com/privacy-policy/"
    static let AtoZdatabasePhoneNumber = "8668322863"
    static let AtoZdatabaseEmailId = "sales@AtoZdatabase.com"
    
    static let GooglePlacesKey = "GooglePlacesAPIKey"
    static let OneSignalKey = "OneSignalAppID"
    static let GoogleAnalyticsTrackingId = "GoogleAnalyticsTrackingId"
    
    static let emptyDetailsControllerId = "EmptyDetailsViewController"
    
    static let mobileCenterAppSecretProd = "1950ae5e-0e0d-4eb5-afdc-1c74e9a7fe1d"
    
    static let mobileCenterAppSecretTest = "47c60bac-f930-467c-b2d3-7768694f77bf"

}


/**
 *  Common mime types used in HTTP request
 */
struct MimeType
{
    private init ()
    {
        
    }
    
    /// Json request / response
    static let Json = "application/json"
    /// Json with data count
    static let JsonWithDataCount = "application/json;fields=data+counts"
    /// GZip Deflate
    static let GZipDeflate = "gzip,deflate"
}

/**
 *  Common headers used in a HTTP request
 */
struct HTTPHeaders {
    
    private init ()
    {
        
    }
    
    static let ContentType = "Content-Type"
    static let AcceptMimeType = "Accept"
    static let AcceptEncoding = "Accept-Encoding"
}

/**
 *  These are closure declarations for different API integrations which give different set of results. All of these completion handlers return a success status, a response message along with appropriate data (if any).
 */
struct Completion {
    
    private init ()
    {
        
    }
    
    /// The defaut completion handler with success status and a response message
    typealias Default = (_ success : Bool, _ responseMessage : String?) -> Void
    
    typealias GetRegistor = (_ success : Bool, _ authData : JSON?, _ responseMessage : String) -> Void

    typealias Authorization = (_ success : Bool, _ authData : JSON?, _ responseMessage : String) -> Void
    
    
    /// Completion for loading the location information
    typealias Location = (_ success : Bool, _ fromLocation : CLLocationCoordinate2D, _ toLocation : CLLocationCoordinate2D, _ responseMessage : String?) -> Void
    
    /// Completion for reverse geocoding a location
    typealias Geocode = (_ success : Bool, _ placemark : CLPlacemark?, _ responseMessage : String?) -> Void
    
}

struct Path {
    static let BusinessNearMe = "nearmebusinesses"
    static let ConsumerNearMe = "nearmeconsumers"
    static let AssignedLead = "/teamlistsbyuserid/"
}

public extension UIDevice {
    
    #if (arch(i386) || arch(x86_64)) && os(iOS)
    static let isSimulator = true
    #else
    static let isSimulator = false
    #endif
    
    var modelName: String {
        
        var machineString = String()
        
        if UIDevice.isSimulator == true
        {
            if let dir = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                machineString = dir
            }
        }
        else {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            machineString = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8 , value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
        }
        switch machineString {
        case "iPod4,1":                                 return "iPod Touch 4G"
        case "iPod5,1":                                 return "iPod Touch 5G"
        case "iPod7,1":                                 return "iPod Touch 6G"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone 9,4":                 return "iPhone 7 Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7 inch)"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9 inch)"
        case "AppleTV5,3":                              return "Apple TV"
        default:                                        return machineString
        }
    }
}

extension String {
    
    /// Returns last path compenent in a string
    var lastPathComponent: String {
        
        get {
            return (self as NSString).lastPathComponent
        }
    }
    
    /// Returns the last string till the first . character starting from the end of string.
    var pathExtension: String {
        
        get {
            
            return (self as NSString).pathExtension
        }
    }
    
    
    var stringByDeletingLastPathComponent: String {
        
        get {
            
            return (self as NSString).deletingLastPathComponent
        }
    }
    var stringByDeletingPathExtension: String {
        
        get {
            
            return (self as NSString).deletingPathExtension
        }
    }
    var pathComponents: [String] {
        
        get {
            
            return (self as NSString).pathComponents
        }
    }
    
    func stringByAppendingPathComponent(_ path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(_ ext: String) -> String? {
        
        let nsSt = self as NSString  
        
        return nsSt.appendingPathExtension(ext)  
    }  
}
