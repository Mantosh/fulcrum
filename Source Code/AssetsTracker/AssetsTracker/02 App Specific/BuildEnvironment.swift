//
//  BuildEnvironment.swift
//  AtoZdatabase
//
//  Created by Mantosh Kumar on 8/22/17.
//  Copyright © 2017 ThirdEye. All rights reserved.
//

import Foundation

/// This defines the current build environment.
enum BuildEnvironment {
    case development, production, adhoc
    
    #if DEVELOPMENT
    static let current = BuildEnvironment.development
    #elseif APPSTORE
    static let current = BuildEnvironment.production
    #else
    static let current = BuildEnvironment.adhoc
    #endif
}
